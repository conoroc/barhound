class AmbienceTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
