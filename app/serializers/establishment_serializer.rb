class EstablishmentSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :location, :main_image_url

  # attributes *Establishment.column_names



  has_many :images
  # has_many :ambience_types
  belongs_to :beer_garden_type


  # def name
  #   "TESTER"
  # end

  def main_image_url
    # UserSerializer.new(object.sender).attributes
    image = object.images.find { |hash| hash[:primary] === true }
    if image
     image.file ? image.file.url : image.url
   end
   
 end
end
