class EstablishmentShowSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :main_image_url, :location, :related, :created_at

  # attributes *Establishment.column_names



  has_many :images
  has_many :ambience_types
  belongs_to :beer_garden_type


  # def name
  #   "TESTER"
  # end

   def main_image_url
    # UserSerializer.new(object.sender).attributes
    image = object.images.find { |hash| hash[:primary] === true }
    if image
     image.file ? image.file.url : image.url
   end
   
 end

  def related
    object.get_related
  end

end
