import { Component } from "@angular/core";
import { Http } from "@angular/http";
import { Router } from "@angular/router";
import { EstablishmentsService } from '../services/establishments';
import { Establishment } from "../models/establishment";
import template from "./template.html";

declare var require: any;

@Component({
    selector: "establishment-index",
    template: require("./template.html"),
    // styleUrls: ['./component2.scss']
    styles: [require("css-to-string-loader!style-loader!./component2.css")]
})


export class EstablishmentIndexComponent {

    keywords: string;
    establishments: Array < Establishment > ;

    constructor(private http: Http,
        private router: Router,
        private establishmentsService: EstablishmentsService) {
        this.establishments = null;
        this.keywords = "";
    }

    ngOnInit(): void {
        const self = this;

        self.establishmentsService.getEstablishments().subscribe((establishments: Array < any > ) => {
            self.establishments = establishments.sort((a, b) => {
                return b.name - a.name;
            });
        });
    }

    updateEstablishments(establishments): void {
        this.establishments = establishments;
    }
}