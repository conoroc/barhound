import { Component, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { Http } from "@angular/http";
import template from "./template.html";


@Component({

    selector: "establishments-filters",
    template: template,
    inputs: [
        "establishments"
    ],
    outputs: [
        "establishmentsChanged"
    ]
})

export class EstablishmentsFiltersComponent {

    keywords: string;
    establishments: Array < any > ;
    establishmentsChanged: EventEmitter < Object > ;

    constructor(private http: Http,
        private router: Router) {
        this.establishments = null;
        this.establishmentsChanged = new EventEmitter();
        this.keywords = "";
    }

    ngOnInit(): void {}

    search($event: any): void {
        const self = this;
        self.keywords = $event || self.keywords;
        // if (self.keywords.length < 3) {
        //     return;
        // }
        self.http.get(
            "/establishments.json?keywords=" + self.keywords
        ).subscribe(
            function(response) {
                self.establishments = response.json();
                self.establishmentsChanged.emit(self.establishments);
            },
            function(error) {
                alert(error);
            }
        );
    }

}