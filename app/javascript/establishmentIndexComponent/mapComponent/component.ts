import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { Http } from "@angular/http";
import { Establishment } from "../../models/establishment";
import template from "./template.html";

@Component({

    selector: "establishments-map",
    template: template
})

export class EstablishmentsMapComponent {

    @Input() establishments: Array < Establishment >;

    latitude: number;
    longitude: number;
    zoom: number;

    constructor(private http: Http,
        private router: Router) {

        // this.establishments = null;
    }

     ngOnInit(): void {
            debugger;
          
            this.latitude = 53.342649;
               this.longitude = -6.260149;
               this.zoom = 12;
        
    }

    // clickedMarker(establishment: any) {
    //     this.router.navigate(["/", establishment.id]);
    // }
}