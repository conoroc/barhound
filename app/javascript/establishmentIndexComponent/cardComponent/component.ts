import { Component, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { Http } from "@angular/http";
import template from "./template.html";

@Component({

    selector: "establishment-card",
    template: template,
    inputs: [
        "establishment"
    ]
})

export class EstablishmentCardComponent {

    establishment: any;

    constructor(private http: Http,
        private router: Router) {
        this.establishment = null;
    }

    ngOnInit(): void {}

    viewDetails(establishment: any) {
        this.router.navigate(["/", establishment.id]);
    }
}