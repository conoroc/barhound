import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Http } from "@angular/http";
import { EstablishmentsService } from '../services/establishments';
import { AgmCoreModule } from '@agm/core';
import { Establishment } from "../models/establishment";
import template from "./template.html";

const mapsCore = AgmCoreModule.forRoot({
    apiKey: 'AIzaSyCqzYlnGLcSsYvd3YN0pBcDedRuuKZLgfo'
});

@Component({
    selector: "establishment-show",
    template: template
})

export class EstablishmentShowComponent {

    keywords: string;
    establishment: Establishment;
    image: any;
    related: Array <Establishment> ;
    id: string;
    url: string;

    constructor(private http: Http,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private establishmentsService: EstablishmentsService) {
        this.id = null;
    }

    ngOnInit(): void {
        const self = this;

        self.activatedRoute.params.subscribe((params: any) => {
            if (params.id) {
                self.establishmentsService.getEstablishment(params.id).subscribe((establishment: any) => {
                  
                    self.establishment = establishment;
                    self.related = self.establishment.related;
                });
            }
        });
        debugger;
        // const establishment =  self.establishmentsService.getEstablishment();
        // console.log(self.activatedRoute.params + '<><>Params<><>');
        //    console.log(establishment + '<><>establishment<><>');


        // const observableFailed = function(error) {
        //     window.alert(error);
        // };
        // const establishmentGetSuccess = function(response) {
        //     self.establishmentsService.getEstablishment();
        //     self.establishment = response.json();
        //     self.related = self.establishment.related;
        //     self.image = self.establishment.main_image || {};
        // };
        // const routeSuccess = function(params) {
        //     console.log('SHOW');

        //     self.id = params.id;
        //     self.http.get(
        //         "/establishments/" + params.id + ".json"
        //     ).subscribe(
        //         establishmentGetSuccess,
        //         observableFailed
        //     );
        // };
        // self.activatedRoute.params.subscribe(routeSuccess, observableFailed);
    }

    clickedMarker() {
        console.log('clicked the marker')
    }
}