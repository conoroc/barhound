import { Component } from "@angular/core";
import { Router } from "@angular/router";
import template from "./template.html";

@Component({
    selector: "related-establishments",
    template: template,
    inputs: [
        "related"
    ]
})

export class RelatedEstablishmentsComponent {

    related: Array <any> ;
    establishment: any;

    constructor(
        private router: Router
    ) {
        this.related = null;
    }

    ngOnInit(): void {
    }

    viewDetails(establishment: any) {
        this.router.navigate(["/", establishment.id]);
    }

}