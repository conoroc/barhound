export class Establishment {
  name: string;
  description: string;
  main_image_url: string;
  related: Array <any> ;
  main_image: object;
  location: any;
}