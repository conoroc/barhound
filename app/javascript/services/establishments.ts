import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { ActivatedRoute, Router } from "@angular/router";
import {Observable} from 'rxjs';
import { Establishment } from "../models/establishment";
 

@Injectable()
 
export class EstablishmentsService {


 
  constructor(
    private http: Http,
    private router: Router) {
  }

    getEstablishments(): Observable<any> {
    // this.request$.emit('starting');
    return this.http.get("/establishments.json")
      .map(response => {
        // this.request$.emit('finished');
        // .map(response => <CarPart[]> response.json() .data);
        return <Establishment[]> response.json();
      })
      .catch(error => this.handleError(error));
  }
 
  getEstablishment(establishmentId: string): Observable<any> {
  	const self = this;

    return this.http.get("/establishments/" + establishmentId + ".json")
      .map(response => {
        // this.request$.emit('finished');
        debugger;
        return <Establishment[]> response.json();
      })
      .catch(error => this.handleError(error));
    // const observableFailed = function(error) {
    //         window.alert(error);
    //     };
    //     const establishmentGetSuccess = function(response) {
     
    //         return response.json();
    
    //     };
    //     const routeSuccess = function(params) {
    //         // console.log(self.activatedRoute.params + '<><>Params<><>');

    //         return self.http.get(
    //             "/establishments/" + 806 + ".json"
    //         ).subscribe(
    //             establishmentGetSuccess,
    //             observableFailed
    //         );
    //     };
    //    return self.activatedRoute.params.subscribe(routeSuccess, observableFailed);
  }
    private handleError(error: any) {
    // this.request$.emit('finished');
    debugger;
    // if (error instanceof Response) {
      // return Observable.throw(error.json()['error'] || 'backend server error');
    // }
    this.router.navigate(["/error/404"]);
    return Observable.throw(error || 'backend server error');
  }

}