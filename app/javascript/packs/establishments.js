import "polyfills";

import {Component, NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";
import {AgmCoreModule} from '@agm/core';

import {EstablishmentsService} from '../services/establishments';

import {EstablishmentIndexComponent} from "../establishmentIndexComponent/component";
import {EstablishmentShowComponent} from "../establishmentShowComponent/component";
import {RelatedEstablishmentsComponent} from "../establishmentShowComponent/relatedEstablishmentsComponent/component";
import {EstablishmentsFiltersComponent} from "../establishmentIndexComponent/filtersComponent/component";
import {EstablishmentCardComponent} from "../establishmentIndexComponent/cardComponent/component";
import {EstablishmentsMapComponent} from "../establishmentIndexComponent/mapComponent/component";
import {NotFoundComponent} from "../errorComponent/component";

const mapsCore = AgmCoreModule.forRoot({
    apiKey: 'AIzaSyCqzYlnGLcSsYvd3YN0pBcDedRuuKZLgfo'
});

const AppComponent = Component({
    selector: "barhound-app",
    template: "<router-outlet></router-outlet>"
}).Class({
    constructor: [
        function () {
        }
    ]
});

const routing = RouterModule.forRoot(
    [
        {
            path: "",
            component: EstablishmentIndexComponent
        },
        {
            path: ":id",
            component: EstablishmentShowComponent
        },
        {path: 'error/404', name: 'NotFound', component: NotFoundComponent},

        {path: '**', redirectTo: ['NotFound']}
    ]);

const EstablishmentsAppModule = NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        mapsCore,
        routing
    ],
    declarations: [
        EstablishmentIndexComponent,
        EstablishmentShowComponent,
        RelatedEstablishmentsComponent,
        EstablishmentsFiltersComponent,
        EstablishmentCardComponent,
        EstablishmentsMapComponent,
        NotFoundComponent,
        AppComponent
    ],
    providers: [EstablishmentsService],
    bootstrap: [AppComponent]
}).Class({
    constructor: function () {
    }
});

platformBrowserDynamic().bootstrapModule(EstablishmentsAppModule);