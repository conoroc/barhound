class Image < ApplicationRecord
	belongs_to :establishment
	mount_uploader :file, ImageUploader	

	scope :main, -> { where(primary: true) }
end
