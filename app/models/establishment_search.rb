class EstablishmentSearch
  attr_reader :where_clause, :where_args, :order

  def initialize(search_term)
    search_term = search_term.downcase
    @where_clause = ""
    @where_args = {}
    # TODO Combine into one
    if search_term =~ /@/
      build_for_email_search(search_term)
    else
      build_for_name_search(search_term)
    end
  end


  private
  def build_for_name_search(search_term)
    @where_clause << case_insensitive_search(:name)
    @where_args[:name] = contains(search_term)
    @where_clause << " OR #{case_insensitive_search(:description)}"
    @where_args[:description] = contains(search_term)
    @order = "name asc"
  end
  def contains(search_term)
    "%" + search_term + "%"
  end
  def case_insensitive_search(field_name)
    "lower(#{field_name}) like :#{field_name}"
  end

  def extract_name(email)
    email.gsub(/@.*$/,'').gsub(/[0-9]+/,'')
  end

  def build_for_email_search(search_term)
    @where_clause << case_insensitive_search(:name)
    @where_args[:name] = contains(extract_name(search_term))
    @where_clause << " OR #{case_insensitive_search(:description)}"
    @where_args[:description] = contains(extract_name(search_term))
    @where_clause << " OR #{case_insensitive_search(:username)}"
    @where_args[:username] = search_term
    @order = "lower(username) = " +
        ActiveRecord::Base.connection.quote(search_term) +
        " desc, name asc"
  end

end