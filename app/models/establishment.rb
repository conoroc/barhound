# name: "#{Faker::Book.title} Bar #{i}",
# location: [Faker::Address.latitude.to_f,Faker::Address.longitude.to_f],
# description: Faker::Simpsons.quote,
# username: "#{Faker::Internet.user_name}#{i}",
# address: {
#   address_1: Faker::Address.street_address,
#   address_2: Faker::Address.street_name,
#   area: Faker::Address.community,
#   city: Faker::Address.city,
#   state: Faker::Address.state,
#   code: Faker::Address.postcode,
#   country: Faker::Address.country
# }


class Establishment < ApplicationRecord

  before_validation :normalize_location

  has_many :images, dependent: :destroy
  accepts_nested_attributes_for :images, allow_destroy: true, reject_if: proc { |attributes| attributes['name'].blank? }

  # has_many :main_images, -> { where({primary: true}) }, class_name: "Image"

 serialize :address, Hash
  serialize :hours, Hash

  store_accessor :address, :address1, :area, :state, :city, :code, :country
  store_accessor :hours, :mon, :tue, :wed, :thu, :fri, :sat, :sun

  belongs_to :beer_garden_type, required: false
  belongs_to :cocktail_type, required: false
  belongs_to :wine_type, required: false
  belongs_to :age_type, required: false
  belongs_to :attire_type, required: false
  belongs_to :outdoor_seating_type, required: false
  belongs_to :noise_level_type, required: false
  belongs_to :parking_type, required: false
  belongs_to :tv_type, required: false
  belongs_to :smoking_area_type, required: false

  has_and_belongs_to_many :ambience_types
  has_and_belongs_to_many :award_types
  has_and_belongs_to_many :bottle_types
  has_and_belongs_to_many :draft_types
  has_and_belongs_to_many :game_types
  has_and_belongs_to_many :sport_types

  scope :index, -> { includes(:images, :beer_garden_type) }
  scope :details, -> { includes(:images, :beer_garden_type, :ambience_types) }

# HOURS?
  # scope :finished, -> { 
  #   where("cards.data->>'finished' = :true", true: "true") 
  # }

# GIVES RESTRICTIONS _ SEE BOOK
 #  enum status: {
 #   signed_up: "signed_up",
 #   verified: "verified",
 #   inactive: "inactive",
 # }

# GOOD FOR FILTERS - DYNAMIC
# scope :most_recent, -> (limit) { order("created_at desc").limit(limit) }

# Establishment.most_recent(5)

  # default_scope {includes(:images, :beer_garden_type, :ambience_types)}
  # default_scope {includes(:images).where(images: { primary: true })}

# attribute :main_image
  # attr_accessor   :main_image

  # def main_image
  #   self.images.first
  # end

  def get_related
    related = Establishment.index.limit(4)
    ActiveModel::SerializableResource.new(related)    
  end

  private

  def normalize_location
    # byebug
    # self.location = [location_x.to_f ,location_y.to_f]
  end

end
