class EstablishmentsController < ApplicationController
  before_action :set_establishment, only: [:edit, :update, :destroy]
  # before_action :format_location, only: [:edit, :update]

  include ActionController::Serialization
  PAGE_SIZE = 50

  def ng
    @base_url = "/establishments/ng"
    render :index
  end


  def index
    @page = (params[:page] || 0).to_i
    #
    if params[:keywords].present?
      @keywords = params[:keywords]
      establishment_search_term = EstablishmentSearch.new(@keywords)
      @establishments = Establishment.where(
        establishment_search_term.where_clause,
        establishment_search_term.where_args).
      order(establishment_search_term.order)
    else
      @establishments = Establishment.index.all.limit(50)
    end

    # @establishments = @establishments.offset(PAGE_SIZE * @page).limit(PAGE_SIZE)

    # puts "><><><><>I<><><>><> #{EstablishmentsPresenter.new(@establishments).as_json}"
    # respond_to do |format|
    #   # format.html {
    #   #   redirect_to "/establishments/ng"
    #   # }
    #   format.json {
    #     # render json: @establishments.to_json(:methods => :main_image)
    #     # render json: {establishments: @establishments.as_json( include: :images )}
    #     # render json: {establishments: @establishments}
    #
    #     render json: @establishments
    #   }
    # end
    respond_to do |format|
      format.html {
        redirect_to establishments_ng_path
      }
      format.json {
        render json: @establishments
      }
    end
  end

  def show
    details_establishment = Establishment.details.find(params[:id])
  # byebug
  respond_to do |format|

    format.json {render json: details_establishment, serializer: EstablishmentShowSerializer}
  end
end
  # GET /establishments/new
  def new
    @establishment = Establishment.new
  end

  # GET /establishments/1/edit
  def edit
    @age_options = AgeType.select(:id, :name).order(:name)
    @ambience_options = AmbienceType.select(:id, :name).order(:name)
    @attire_options = AttireType.select(:id, :name).order(:name)
    @bottle_options = BottleType.select(:id, :name).order(:name)
    @draft_options = DraftType.select(:id, :name).order(:name)
    @game_options = GameType.select(:id, :name).order(:name)
     @award_options = AwardType.select(:id, :name).order(:name)
    @sport_options = SportType.select(:id, :name).order(:name)
    @outdoor_seating_options = OutdoorSeatingType.select(:id, :name).order(:name)
    @noise_level_options = NoiseLevelType.select(:id, :name).order(:name)
    @parking_options = ParkingType.select(:id, :name).order(:name)
    @wine_options = WineType.select(:id, :name).order(:name)
    @tv_options = TvType.select(:id, :name).order(:name)
    @cocktail_options = CocktailType.select(:id, :name).order(:name)

    @beer_garden__options = BeerGardenType.select(:id, :name).order(:name)
    @smoking_area_options = SmokingAreaType.select(:id, :name).order(:name)


    # @images = @establishment.images.build
    @existing_images = @establishment.images
    @images = @establishment.images.build

 end

  # POST /establishments
  # POST /establishments.json
  def create
    @establishment = Establishment.new(establishment_params)

    respond_to do |format|
      if @establishment.save
        format.html { redirect_to @establishment, notice: 'Establishment was successfully created.' }
        format.json { render :show, status: :created, location: @establishment }
      else
        format.html { render :new }
        format.json { render json: @establishment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /establishments/1
  # PATCH/PUT /establishments/1.json
  def update
    respond_to do |format|

      format_location
      params[:establishment].merge!(hours:params[:hours])
      params[:establishment].merge!(address:params[:address])
      # byebug
# # point = [params[:establishment][:location_x].to_f, params[:establishment][:location_y].to_f] 
# params[:establishment].delete(:location_x)
# params[:establishment].delete(:location_y)
# params[:establishment][:location] = point
      # establishment_params['location'] = [params[:location_x].to_f, params[:location_y].to_f]  
# byebug
if @establishment.update(establishment_params)
  format.html { redirect_to "#{establishments_ng_path}/#{@establishment.id}", notice: 'Establishment was successfully updated.' }
  format.json { redirect_to "#{establishments_ng_path}/#{@establishment.id}", status: :ok, location: @establishment }
else
  format.html { render :edit }
  format.json { render json: @establishment.errors, status: :unprocessable_entity }
end
end
end

  # DELETE /establishments/1
  # DELETE /establishments/1.json
  def destroy
    @establishment.destroy
    respond_to do |format|
      format.html { redirect_to establishments_url, notice: 'Establishment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_establishment
      @establishment = Establishment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def establishment_params
      # params.require(:establishment).permit!
      # params.require(:establishment).permit(address: {})
      params.require(:establishment).permit(
        :name,
        :phone_number, 
        :username, 
        :email, 
        :facebook, 
        :twitter, 
        :yelp, 
        :website, 
        :tags, 
        :music_types, 
        :best_nights, 
        :description, 
        :menu_link, 
        :suits_dancing, 
        :suits_kids, 
        :suits_groups, 
        :drinks_link, 
        :cred_card, 
        :sells_cigs, 
        :free_wifi, 
        :accessible, 
        :high_chair, 
        :coat_check, 
        :dog_friendly, 
        :price, 
        :notes, 
        :founded, 
        :smoking_area_type_id, 
        :beer_garden_type_id, 
        :cocktail_type_id, 
        :tv_type_id, 
        :wine_type_id, 
        :age_type_id, 
        :attire_type_id, 
        :outdoor_seating_type_id, 
        :noise_level_type_id, 
        :parking_type_id, 
        location: [],
        ambience_type_ids: [],
        award_type_ids: [], 
        bottle_type_ids: [], 
        draft_type_ids: [], 
        game_type_ids: [], 
        sport_type_ids: [],
        images_attributes: [:id, :name, :file, :primary, :_destroy],
        address: {},
        hours: {})
    end

    def format_location
      location_coords = [params[:location_x].to_f, params[:location_y].to_f]  
      params[:establishment].merge!(location:location_coords)
    end

  end
