class EstablishmentsController < ApplicationController
  include ActionController::Serialization
  PAGE_SIZE = 50

  def ng
    @base_url = "/establishments/ng"
    render :index
  end

  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def index
    @page = (params[:page] || 0).to_i
    #
    if params[:keywords].present?
      @keywords = params[:keywords]
      establishment_search_term = EstablishmentSearch.new(@keywords)
      @establishments = Establishment.where(
          establishment_search_term.where_clause,
          establishment_search_term.where_args).
          order(establishment_search_term.order)
    else
      @establishments = Establishment.index.all.limit(50)
    end

    # @establishments = @establishments.offset(PAGE_SIZE * @page).limit(PAGE_SIZE)

    # puts "><><><><>I<><><>><> #{EstablishmentsPresenter.new(@establishments).as_json}"
    # respond_to do |format|
    #   # format.html {
    #   #   redirect_to "/establishments/ng"
    #   # }
    #   format.json {
    #     # render json: @establishments.to_json(:methods => :main_image)
    #     # render json: {establishments: @establishments.as_json( include: :images )}
    #     # render json: {establishments: @establishments}
    #
    #     render json: @establishments
    #   }
    # end
    respond_to do |format|
      format.html {
        redirect_to establishments_ng_path
      }
      format.json {
        render json: @establishments
      }
    end
  end

  def show
    establishment = Establishment.details.find(params[:id])

    respond_to do |format|

      format.json {render json: establishment, serializer: EstablishmentShowSerializer}
    end
  end
end
