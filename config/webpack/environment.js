const { environment } = require('@rails/webpacker');
const webpack = require('webpack')



environment.loaders.set('babel', {
	test: /\.js$/,
	loader: 'babel-loader'
});

environment.loaders.set('html', {
    test: /\.html$/,
    use: [{
        loader: 'html-loader',
        options: {
            minimize: true,
            removeAttributeQuotes: false,
            caseSensitive: true,
            customAttrSurround: [ [/#/, /(?:)/], [/\*/, /(?:)/], [/\[?\(?/, /(?:)/] ],
            customAttrAssign: [ /\)?\]?=/ ]
        }
    }]
});

// environment.loaders.set('style', {
//     test: /\.scss$/,
//     use: [{
//         loader: 'sass-loader',
//         options: {
//             minimize: true,
//             removeAttributeQuotes: false,
//             caseSensitive: true,
//             customAttrSurround: [ [/#/, /(?:)/], [/\*/, /(?:)/], [/\[?\(?/, /(?:)/] ],
//             customAttrAssign: [ /\)?\]?=/ ]
//         }
//     }]
// });

// environment.loaders.set('style', {
//     "test": /\.(scss|sass|css)$/,
//     "use": [
//     { loader: "to-string-loader" },
//      {
//             loader: "css-loader",
//             options: { minimize: false }
//         },{
//             loader: "/home/conor/Projects/Rails/barhound/node_modules/extract-text-webpack-plugin/dist/loader.js",
//             options: { omit: 1, remove: true }
//         },
//         { loader: "style-loader" },
       
//         {
//             loader: "postcss-loader",
//             options: {
//                 sourceMap: true,
//                 config: { "path": "/home/conor/Projects/Rails/barhound/.postcssrc.yml" }
//             }
//         },
//         { loader: "resolve-url-loader" },
//         {
//             loader: "sass-loader",
//             options: { sourceMap: true }
//         }
//     ]
//     // test: /\.css$/, loaders: ['to-string-loader', 'css-loader'] 
//     // test: /\.scss$/, loaders: ['to-string-loader', 'css-loader'] 
// });

environment.plugins.set(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    'window.Tether': 'tether',
    Popper: ['popper.js', 'default'],
    ActionCable: 'actioncable',
    Vue: 'vue',
    VueResource: 'vue-resource',
  })
)



// const merge = require('webpack-merge')
//
// const myCssLoaderOptions = {
//     modules: true,
//     sourceMap: true,
//     importLoaders: 1,
// }
//
// const CSSLoader = environment.loaders.get('style').use.find(el => el.loader === 'css-loader')
//
// CSSLoader.options = merge(CSSLoader.options, myCssLoaderOptions)
//
console.log('<><><>' + JSON.stringify(environment.loaders.get('style')));

module.exports = environment;
