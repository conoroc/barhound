Rails.application.routes.draw do

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "establishments#index"
  # These supercede other /establishments routes, so must
  # come before resource :establishments
  get "establishments/ng", to: "establishments#ng"
  get "establishments/ng/*angular_route", to: "establishments#ng"


  resources :establishments

  # get "angular_test" => "angular_test#index"



end
