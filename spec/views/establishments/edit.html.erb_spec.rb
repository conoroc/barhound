require 'rails_helper'

RSpec.describe "establishments/edit", type: :view do
  before(:each) do
    @establishment = assign(:establishment, Establishment.create!(
      :name => "MyString",
      :phone_number => "MyString",
      :location => "",
      :address => "",
      :username => "MyString",
      :email => "MyString",
      :facebook => "MyString",
      :twitter => "MyString",
      :yelp => "MyString",
      :website => "MyString",
      :tags => "MyString",
      :music_types => "MyString",
      :best_nights => "MyString",
      :description => "MyString",
      :menu_link => "MyString",
      :suits_dancing => 1,
      :suits_kids => 1,
      :suits_groups => 1,
      :drinks_link => "MyString",
      :cred_card => false,
      :sells_cigs => false,
      :free_wifi => false,
      :accessible => false,
      :high_chair => false,
      :coat_check => false,
      :dog_friendly => false,
      :price => 1,
      :notes => "MyString",
      :smoking_area_type => nil,
      :beer_garden_type => nil,
      :cocktail_type => nil,
      :tv_type => nil,
      :wine_type => nil,
      :age_type => nil,
      :attire_type => nil,
      :outdoor_seating_type => nil,
      :noise_level_type => nil,
      :parking_type => nil,
      :hours => ""
    ))
  end

  it "renders the edit establishment form" do
    render

    assert_select "form[action=?][method=?]", establishment_path(@establishment), "post" do

      assert_select "input[name=?]", "establishment[name]"

      assert_select "input[name=?]", "establishment[phone_number]"

      assert_select "input[name=?]", "establishment[location]"

      assert_select "input[name=?]", "establishment[address]"

      assert_select "input[name=?]", "establishment[username]"

      assert_select "input[name=?]", "establishment[email]"

      assert_select "input[name=?]", "establishment[facebook]"

      assert_select "input[name=?]", "establishment[twitter]"

      assert_select "input[name=?]", "establishment[yelp]"

      assert_select "input[name=?]", "establishment[website]"

      assert_select "input[name=?]", "establishment[tags]"

      assert_select "input[name=?]", "establishment[music_types]"

      assert_select "input[name=?]", "establishment[best_nights]"

      assert_select "input[name=?]", "establishment[description]"

      assert_select "input[name=?]", "establishment[menu_link]"

      assert_select "input[name=?]", "establishment[suits_dancing]"

      assert_select "input[name=?]", "establishment[suits_kids]"

      assert_select "input[name=?]", "establishment[suits_groups]"

      assert_select "input[name=?]", "establishment[drinks_link]"

      assert_select "input[name=?]", "establishment[cred_card]"

      assert_select "input[name=?]", "establishment[sells_cigs]"

      assert_select "input[name=?]", "establishment[free_wifi]"

      assert_select "input[name=?]", "establishment[accessible]"

      assert_select "input[name=?]", "establishment[high_chair]"

      assert_select "input[name=?]", "establishment[coat_check]"

      assert_select "input[name=?]", "establishment[dog_friendly]"

      assert_select "input[name=?]", "establishment[price]"

      assert_select "input[name=?]", "establishment[notes]"

      assert_select "input[name=?]", "establishment[smoking_area_type_id]"

      assert_select "input[name=?]", "establishment[beer_garden_type_id]"

      assert_select "input[name=?]", "establishment[cocktail_type_id]"

      assert_select "input[name=?]", "establishment[tv_type_id]"

      assert_select "input[name=?]", "establishment[wine_type_id]"

      assert_select "input[name=?]", "establishment[age_type_id]"

      assert_select "input[name=?]", "establishment[attire_type_id]"

      assert_select "input[name=?]", "establishment[outdoor_seating_type_id]"

      assert_select "input[name=?]", "establishment[noise_level_type_id]"

      assert_select "input[name=?]", "establishment[parking_type_id]"

      assert_select "input[name=?]", "establishment[hours]"
    end
  end
end
