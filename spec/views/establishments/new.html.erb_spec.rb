require 'rails_helper'

RSpec.describe "establishments/new", type: :view do
  before(:each) do
    assign(:establishment, Establishment.new())
  end

  it "renders new establishment form" do
    render

    assert_select "form[action=?][method=?]", establishments_path, "post" do
    end
  end
end
