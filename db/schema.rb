# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171026124038) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "age_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "ambience_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "ambience_types_establishments", id: false, force: :cascade do |t|
    t.bigint "ambience_type_id", null: false
    t.bigint "establishment_id", null: false
    t.index ["ambience_type_id", "establishment_id"], name: "ambience_type_establishment_index"
  end

  create_table "attire_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "award_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "award_types_establishments", id: false, force: :cascade do |t|
    t.bigint "award_type_id", null: false
    t.bigint "establishment_id", null: false
    t.index ["award_type_id", "establishment_id"], name: "award_type_establishment_index"
  end

  create_table "beer_garden_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "bottle_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "bottle_types_establishments", id: false, force: :cascade do |t|
    t.bigint "bottle_type_id", null: false
    t.bigint "establishment_id", null: false
    t.index ["bottle_type_id", "establishment_id"], name: "bottle_type_establishment_index"
  end

  create_table "cocktail_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "draft_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "draft_types_establishments", id: false, force: :cascade do |t|
    t.bigint "draft_type_id", null: false
    t.bigint "establishment_id", null: false
    t.index ["draft_type_id", "establishment_id"], name: "draft_type_establishment_index"
  end

  create_table "establishments", force: :cascade do |t|
    t.string "name", null: false
    t.string "phone_number"
    t.point "location"
    t.jsonb "address", default: "{}", null: false
    t.string "username"
    t.string "email"
    t.string "facebook"
    t.string "twitter"
    t.string "yelp"
    t.string "website"
    t.string "tags", default: [], array: true
    t.string "music_types", default: [], array: true
    t.string "best_nights", default: [], array: true
    t.string "description"
    t.string "menu_link"
    t.integer "suits_dancing"
    t.integer "suits_kids"
    t.integer "suits_groups"
    t.string "drinks_link"
    t.boolean "cred_card"
    t.boolean "sells_cigs"
    t.boolean "free_wifi"
    t.boolean "accessible"
    t.boolean "high_chair"
    t.boolean "coat_check"
    t.boolean "dog_friendly"
    t.integer "price"
    t.string "notes", default: [], array: true
    t.date "founded"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "smoking_area_type_id"
    t.bigint "beer_garden_type_id"
    t.bigint "cocktail_type_id"
    t.bigint "tv_type_id"
    t.bigint "wine_type_id"
    t.bigint "age_type_id"
    t.bigint "attire_type_id"
    t.bigint "outdoor_seating_type_id"
    t.bigint "noise_level_type_id"
    t.bigint "parking_type_id"
    t.jsonb "hours", default: {}
    t.index "lower((description)::text) varchar_pattern_ops", name: "index_establishments_on_lower_description_varchar_pattern_ops"
    t.index "lower((name)::text) varchar_pattern_ops", name: "index_establishments_on_lower_name_varchar_pattern_ops"
    t.index ["address"], name: "index_establishments_on_address", using: :gin
    t.index ["age_type_id"], name: "index_establishments_on_age_type_id"
    t.index ["attire_type_id"], name: "index_establishments_on_attire_type_id"
    t.index ["beer_garden_type_id"], name: "index_establishments_on_beer_garden_type_id"
    t.index ["cocktail_type_id"], name: "index_establishments_on_cocktail_type_id"
    t.index ["description"], name: "index_establishments_on_description"
    t.index ["hours"], name: "index_establishments_on_hours", using: :gin
    t.index ["music_types"], name: "index_establishments_on_music_types", using: :gin
    t.index ["noise_level_type_id"], name: "index_establishments_on_noise_level_type_id"
    t.index ["notes"], name: "index_establishments_on_notes", using: :gin
    t.index ["outdoor_seating_type_id"], name: "index_establishments_on_outdoor_seating_type_id"
    t.index ["parking_type_id"], name: "index_establishments_on_parking_type_id"
    t.index ["smoking_area_type_id"], name: "index_establishments_on_smoking_area_type_id"
    t.index ["tags"], name: "index_establishments_on_tags", using: :gin
    t.index ["tv_type_id"], name: "index_establishments_on_tv_type_id"
    t.index ["wine_type_id"], name: "index_establishments_on_wine_type_id"
  end

  create_table "establishments_game_types", id: false, force: :cascade do |t|
    t.bigint "game_type_id", null: false
    t.bigint "establishment_id", null: false
    t.index ["game_type_id", "establishment_id"], name: "game_type_establishment_index"
  end

  create_table "establishments_sport_types", id: false, force: :cascade do |t|
    t.bigint "sport_type_id", null: false
    t.bigint "establishment_id", null: false
    t.index ["sport_type_id", "establishment_id"], name: "sport_type_establishment_index"
  end

  create_table "game_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "images", force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.string "file"
    t.bigint "establishment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "primary", default: false
    t.index ["establishment_id"], name: "index_images_on_establishment_id"
  end

  create_table "noise_level_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "outdoor_seating_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "parking_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "smoking_area_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "sport_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "tv_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "wine_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type_id"
  end

  add_foreign_key "establishments", "age_types"
  add_foreign_key "establishments", "attire_types"
  add_foreign_key "establishments", "beer_garden_types"
  add_foreign_key "establishments", "cocktail_types"
  add_foreign_key "establishments", "noise_level_types"
  add_foreign_key "establishments", "outdoor_seating_types"
  add_foreign_key "establishments", "parking_types"
  add_foreign_key "establishments", "smoking_area_types"
  add_foreign_key "establishments", "tv_types"
  add_foreign_key "establishments", "wine_types"
  add_foreign_key "images", "establishments"
end
