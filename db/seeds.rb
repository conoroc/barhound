# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Image.delete_all
# Establishment.delete_all

# @est= Establishment.all

# hours = {mon: {open: '1200', close: '2330'}, tue: {open: '1200', close: '2330'}, wed: {open: '1200', close: '2330'}, thu: {open: '1200', close: '0200'}, fri: {open: '1200', close: '0200'}, sat: {open: '1200', close: '0200'}, sun: {open: '1200', close: '2300'}}
# @est.each do |i|
#   @est = i.update!({hours: hours})
#   puts ".......#{i.id}"
# end

# again, it won't blow up or create a duplicate state
# State.find_or_create_by!(name: "Alabama" , code: "AL")



@beer_garden_type = BeerGardenType.where({type_id: 'small'}).first
@ambience_type1 = AmbienceType.first
@ambience_type2 = AmbienceType.last
100.times do |i|
  @est = Establishment.create!(
      name: "#{Faker::Book.title} Bar #{i}",
      location: [Faker::Address.latitude.to_f,Faker::Address.longitude.to_f],
      description: Faker::Seinfeld.quote,
      email: Faker::Internet.safe_email,
      phone_number: Faker::PhoneNumber.cell_phone,
      website: Faker::Internet.url,
      facebook: Faker::Internet.url,
      username: "#{Faker::Internet.user_name}#{i}",
      beer_garden_type_id: @beer_garden_type.id,
      address: {
          address_1: Faker::Address.street_address,
          address_2: Faker::Address.street_name,
          area: Faker::Address.community,
          city: Faker::Address.city,
          state: Faker::Address.state,
          code: Faker::Address.postcode,
          country: Faker::Address.country
      })

  print "..... #{@est.beer_garden_type_id}"
  @est.ambience_types << @ambience_type1
  @est.ambience_types << @ambience_type2
  @image = Image.create!(
      name: Faker::Coffee.blend_name,
      url: "http://pipsum.com/750x500",
      establishment_id: @est.id,
      primary: true
  )

  print '.'
end
puts


