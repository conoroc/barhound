SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: age_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE age_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: age_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE age_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: age_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE age_types_id_seq OWNED BY age_types.id;


--
-- Name: ambience_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ambience_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: ambience_types_establishments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ambience_types_establishments (
    ambience_type_id bigint NOT NULL,
    establishment_id bigint NOT NULL
);


--
-- Name: ambience_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ambience_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ambience_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ambience_types_id_seq OWNED BY ambience_types.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: attire_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE attire_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: attire_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attire_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attire_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE attire_types_id_seq OWNED BY attire_types.id;


--
-- Name: award_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE award_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: award_types_establishments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE award_types_establishments (
    award_type_id bigint NOT NULL,
    establishment_id bigint NOT NULL
);


--
-- Name: award_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE award_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: award_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE award_types_id_seq OWNED BY award_types.id;


--
-- Name: beer_garden_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE beer_garden_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: beer_garden_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE beer_garden_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beer_garden_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE beer_garden_types_id_seq OWNED BY beer_garden_types.id;


--
-- Name: bottle_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE bottle_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: bottle_types_establishments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE bottle_types_establishments (
    bottle_type_id bigint NOT NULL,
    establishment_id bigint NOT NULL
);


--
-- Name: bottle_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bottle_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bottle_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bottle_types_id_seq OWNED BY bottle_types.id;


--
-- Name: cocktail_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cocktail_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: cocktail_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cocktail_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cocktail_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cocktail_types_id_seq OWNED BY cocktail_types.id;


--
-- Name: draft_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE draft_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: draft_types_establishments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE draft_types_establishments (
    draft_type_id bigint NOT NULL,
    establishment_id bigint NOT NULL
);


--
-- Name: draft_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE draft_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: draft_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE draft_types_id_seq OWNED BY draft_types.id;


--
-- Name: establishments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE establishments (
    id bigint NOT NULL,
    name character varying NOT NULL,
    phone_number character varying,
    location point,
    address jsonb DEFAULT '"{}"'::jsonb NOT NULL,
    username character varying,
    email character varying,
    facebook character varying,
    twitter character varying,
    yelp character varying,
    website character varying,
    tags character varying[] DEFAULT '{}'::character varying[],
    music_types character varying[] DEFAULT '{}'::character varying[],
    best_nights character varying[] DEFAULT '{}'::character varying[],
    description character varying,
    menu_link character varying,
    suits_dancing integer,
    suits_kids integer,
    suits_groups integer,
    drinks_link character varying,
    cred_card boolean,
    sells_cigs boolean,
    free_wifi boolean,
    accessible boolean,
    high_chair boolean,
    coat_check boolean,
    dog_friendly boolean,
    price integer,
    notes character varying[] DEFAULT '{}'::character varying[],
    founded date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    smoking_area_type_id bigint,
    beer_garden_type_id bigint,
    cocktail_type_id bigint,
    tv_type_id bigint,
    wine_type_id bigint,
    age_type_id bigint,
    attire_type_id bigint,
    outdoor_seating_type_id bigint,
    noise_level_type_id bigint,
    parking_type_id bigint,
    hours jsonb DEFAULT '{}'::jsonb
);


--
-- Name: establishments_game_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE establishments_game_types (
    game_type_id bigint NOT NULL,
    establishment_id bigint NOT NULL
);


--
-- Name: establishments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE establishments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: establishments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE establishments_id_seq OWNED BY establishments.id;


--
-- Name: establishments_sport_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE establishments_sport_types (
    sport_type_id bigint NOT NULL,
    establishment_id bigint NOT NULL
);


--
-- Name: game_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: game_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE game_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: game_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE game_types_id_seq OWNED BY game_types.id;


--
-- Name: images; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE images (
    id bigint NOT NULL,
    name character varying,
    url character varying,
    file character varying,
    establishment_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    "primary" boolean DEFAULT false
);


--
-- Name: images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE images_id_seq OWNED BY images.id;


--
-- Name: noise_level_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE noise_level_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: noise_level_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE noise_level_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: noise_level_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE noise_level_types_id_seq OWNED BY noise_level_types.id;


--
-- Name: outdoor_seating_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE outdoor_seating_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: outdoor_seating_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE outdoor_seating_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: outdoor_seating_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE outdoor_seating_types_id_seq OWNED BY outdoor_seating_types.id;


--
-- Name: parking_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE parking_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: parking_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE parking_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parking_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE parking_types_id_seq OWNED BY parking_types.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: smoking_area_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE smoking_area_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: smoking_area_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE smoking_area_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: smoking_area_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE smoking_area_types_id_seq OWNED BY smoking_area_types.id;


--
-- Name: sport_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sport_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: sport_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sport_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sport_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sport_types_id_seq OWNED BY sport_types.id;


--
-- Name: tv_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tv_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: tv_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tv_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tv_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tv_types_id_seq OWNED BY tv_types.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: wine_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE wine_types (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    type_id character varying
);


--
-- Name: wine_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE wine_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wine_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE wine_types_id_seq OWNED BY wine_types.id;


--
-- Name: age_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY age_types ALTER COLUMN id SET DEFAULT nextval('age_types_id_seq'::regclass);


--
-- Name: ambience_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ambience_types ALTER COLUMN id SET DEFAULT nextval('ambience_types_id_seq'::regclass);


--
-- Name: attire_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY attire_types ALTER COLUMN id SET DEFAULT nextval('attire_types_id_seq'::regclass);


--
-- Name: award_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY award_types ALTER COLUMN id SET DEFAULT nextval('award_types_id_seq'::regclass);


--
-- Name: beer_garden_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY beer_garden_types ALTER COLUMN id SET DEFAULT nextval('beer_garden_types_id_seq'::regclass);


--
-- Name: bottle_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bottle_types ALTER COLUMN id SET DEFAULT nextval('bottle_types_id_seq'::regclass);


--
-- Name: cocktail_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cocktail_types ALTER COLUMN id SET DEFAULT nextval('cocktail_types_id_seq'::regclass);


--
-- Name: draft_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_types ALTER COLUMN id SET DEFAULT nextval('draft_types_id_seq'::regclass);


--
-- Name: establishments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments ALTER COLUMN id SET DEFAULT nextval('establishments_id_seq'::regclass);


--
-- Name: game_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_types ALTER COLUMN id SET DEFAULT nextval('game_types_id_seq'::regclass);


--
-- Name: images id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY images ALTER COLUMN id SET DEFAULT nextval('images_id_seq'::regclass);


--
-- Name: noise_level_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY noise_level_types ALTER COLUMN id SET DEFAULT nextval('noise_level_types_id_seq'::regclass);


--
-- Name: outdoor_seating_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY outdoor_seating_types ALTER COLUMN id SET DEFAULT nextval('outdoor_seating_types_id_seq'::regclass);


--
-- Name: parking_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY parking_types ALTER COLUMN id SET DEFAULT nextval('parking_types_id_seq'::regclass);


--
-- Name: smoking_area_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY smoking_area_types ALTER COLUMN id SET DEFAULT nextval('smoking_area_types_id_seq'::regclass);


--
-- Name: sport_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sport_types ALTER COLUMN id SET DEFAULT nextval('sport_types_id_seq'::regclass);


--
-- Name: tv_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tv_types ALTER COLUMN id SET DEFAULT nextval('tv_types_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: wine_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY wine_types ALTER COLUMN id SET DEFAULT nextval('wine_types_id_seq'::regclass);


--
-- Name: age_types age_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY age_types
    ADD CONSTRAINT age_types_pkey PRIMARY KEY (id);


--
-- Name: ambience_types ambience_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ambience_types
    ADD CONSTRAINT ambience_types_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: attire_types attire_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attire_types
    ADD CONSTRAINT attire_types_pkey PRIMARY KEY (id);


--
-- Name: award_types award_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY award_types
    ADD CONSTRAINT award_types_pkey PRIMARY KEY (id);


--
-- Name: beer_garden_types beer_garden_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY beer_garden_types
    ADD CONSTRAINT beer_garden_types_pkey PRIMARY KEY (id);


--
-- Name: bottle_types bottle_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bottle_types
    ADD CONSTRAINT bottle_types_pkey PRIMARY KEY (id);


--
-- Name: cocktail_types cocktail_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cocktail_types
    ADD CONSTRAINT cocktail_types_pkey PRIMARY KEY (id);


--
-- Name: draft_types draft_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY draft_types
    ADD CONSTRAINT draft_types_pkey PRIMARY KEY (id);


--
-- Name: establishments establishments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT establishments_pkey PRIMARY KEY (id);


--
-- Name: game_types game_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_types
    ADD CONSTRAINT game_types_pkey PRIMARY KEY (id);


--
-- Name: images images_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: noise_level_types noise_level_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY noise_level_types
    ADD CONSTRAINT noise_level_types_pkey PRIMARY KEY (id);


--
-- Name: outdoor_seating_types outdoor_seating_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY outdoor_seating_types
    ADD CONSTRAINT outdoor_seating_types_pkey PRIMARY KEY (id);


--
-- Name: parking_types parking_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY parking_types
    ADD CONSTRAINT parking_types_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: smoking_area_types smoking_area_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY smoking_area_types
    ADD CONSTRAINT smoking_area_types_pkey PRIMARY KEY (id);


--
-- Name: sport_types sport_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sport_types
    ADD CONSTRAINT sport_types_pkey PRIMARY KEY (id);


--
-- Name: tv_types tv_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tv_types
    ADD CONSTRAINT tv_types_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: wine_types wine_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY wine_types
    ADD CONSTRAINT wine_types_pkey PRIMARY KEY (id);


--
-- Name: ambience_type_establishment_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ambience_type_establishment_index ON ambience_types_establishments USING btree (ambience_type_id, establishment_id);


--
-- Name: award_type_establishment_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX award_type_establishment_index ON award_types_establishments USING btree (award_type_id, establishment_id);


--
-- Name: bottle_type_establishment_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bottle_type_establishment_index ON bottle_types_establishments USING btree (bottle_type_id, establishment_id);


--
-- Name: draft_type_establishment_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX draft_type_establishment_index ON draft_types_establishments USING btree (draft_type_id, establishment_id);


--
-- Name: game_type_establishment_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX game_type_establishment_index ON establishments_game_types USING btree (game_type_id, establishment_id);


--
-- Name: index_establishments_on_address; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_address ON establishments USING gin (address);


--
-- Name: index_establishments_on_age_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_age_type_id ON establishments USING btree (age_type_id);


--
-- Name: index_establishments_on_attire_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_attire_type_id ON establishments USING btree (attire_type_id);


--
-- Name: index_establishments_on_beer_garden_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_beer_garden_type_id ON establishments USING btree (beer_garden_type_id);


--
-- Name: index_establishments_on_cocktail_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_cocktail_type_id ON establishments USING btree (cocktail_type_id);


--
-- Name: index_establishments_on_description; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_description ON establishments USING btree (description);


--
-- Name: index_establishments_on_hours; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_hours ON establishments USING gin (hours);


--
-- Name: index_establishments_on_lower_description_varchar_pattern_ops; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_lower_description_varchar_pattern_ops ON establishments USING btree (lower((description)::text) varchar_pattern_ops);


--
-- Name: index_establishments_on_lower_name_varchar_pattern_ops; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_lower_name_varchar_pattern_ops ON establishments USING btree (lower((name)::text) varchar_pattern_ops);


--
-- Name: index_establishments_on_music_types; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_music_types ON establishments USING gin (music_types);


--
-- Name: index_establishments_on_noise_level_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_noise_level_type_id ON establishments USING btree (noise_level_type_id);


--
-- Name: index_establishments_on_notes; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_notes ON establishments USING gin (notes);


--
-- Name: index_establishments_on_outdoor_seating_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_outdoor_seating_type_id ON establishments USING btree (outdoor_seating_type_id);


--
-- Name: index_establishments_on_parking_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_parking_type_id ON establishments USING btree (parking_type_id);


--
-- Name: index_establishments_on_smoking_area_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_smoking_area_type_id ON establishments USING btree (smoking_area_type_id);


--
-- Name: index_establishments_on_tags; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_tags ON establishments USING gin (tags);


--
-- Name: index_establishments_on_tv_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_tv_type_id ON establishments USING btree (tv_type_id);


--
-- Name: index_establishments_on_wine_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_establishments_on_wine_type_id ON establishments USING btree (wine_type_id);


--
-- Name: index_images_on_establishment_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_images_on_establishment_id ON images USING btree (establishment_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: sport_type_establishment_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sport_type_establishment_index ON establishments_sport_types USING btree (sport_type_id, establishment_id);


--
-- Name: establishments fk_rails_29da2d4956; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_29da2d4956 FOREIGN KEY (parking_type_id) REFERENCES parking_types(id);


--
-- Name: establishments fk_rails_2f8221eae9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_2f8221eae9 FOREIGN KEY (cocktail_type_id) REFERENCES cocktail_types(id);


--
-- Name: establishments fk_rails_3ccdc97bee; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_3ccdc97bee FOREIGN KEY (beer_garden_type_id) REFERENCES beer_garden_types(id);


--
-- Name: establishments fk_rails_5e8920f64b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_5e8920f64b FOREIGN KEY (wine_type_id) REFERENCES wine_types(id);


--
-- Name: establishments fk_rails_7235d0ad62; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_7235d0ad62 FOREIGN KEY (age_type_id) REFERENCES age_types(id);


--
-- Name: establishments fk_rails_8f5a7e8da1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_8f5a7e8da1 FOREIGN KEY (outdoor_seating_type_id) REFERENCES outdoor_seating_types(id);


--
-- Name: establishments fk_rails_a09891ca01; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_a09891ca01 FOREIGN KEY (tv_type_id) REFERENCES tv_types(id);


--
-- Name: establishments fk_rails_aedb5d8fe6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_aedb5d8fe6 FOREIGN KEY (noise_level_type_id) REFERENCES noise_level_types(id);


--
-- Name: establishments fk_rails_c0697f7be4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_c0697f7be4 FOREIGN KEY (smoking_area_type_id) REFERENCES smoking_area_types(id);


--
-- Name: images fk_rails_c2fa18ed57; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY images
    ADD CONSTRAINT fk_rails_c2fa18ed57 FOREIGN KEY (establishment_id) REFERENCES establishments(id);


--
-- Name: establishments fk_rails_dac390216f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY establishments
    ADD CONSTRAINT fk_rails_dac390216f FOREIGN KEY (attire_type_id) REFERENCES attire_types(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20170927095931'),
('20170927124000'),
('20170928140729'),
('20171004124810'),
('20171004124858'),
('20171004124936'),
('20171004124955'),
('20171004125045'),
('20171004125054'),
('20171004125109'),
('20171004125124'),
('20171004125142'),
('20171004125155'),
('20171004125234'),
('20171004133343'),
('20171004160844'),
('20171014151211'),
('20171014151641'),
('20171023145740'),
('20171026124038');


