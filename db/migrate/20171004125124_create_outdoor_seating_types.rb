class CreateOutdoorSeatingTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :outdoor_seating_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end
  end
end
