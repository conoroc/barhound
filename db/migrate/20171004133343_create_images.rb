class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :name
      t.string :url
      t.string :file
      t.references :establishment, foreign_key: true

      t.timestamps
    end
  end
end
