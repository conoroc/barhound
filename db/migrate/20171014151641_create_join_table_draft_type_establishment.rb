class CreateJoinTableDraftTypeEstablishment < ActiveRecord::Migration[5.1]
  def change
    create_join_table :draft_types, :establishments do |t|
      t.index [:draft_type_id, :establishment_id], name: :draft_type_establishment_index
      # t.index [:establishment_id, :draft_type_id]
    end

     create_join_table :game_types, :establishments do |t|
      t.index [:game_type_id, :establishment_id], name: :game_type_establishment_index
      # t.index [:establishment_id, :draft_type_id]
    end

     create_join_table :ambience_types, :establishments do |t|
      t.index [:ambience_type_id, :establishment_id], name: :ambience_type_establishment_index
      # t.index [:establishment_id, :draft_type_id]
    end

     create_join_table :award_types, :establishments do |t|
      t.index [:award_type_id, :establishment_id], name: :award_type_establishment_index
      # t.index [:establishment_id, :draft_type_id]
    end

     create_join_table :bottle_types, :establishments do |t|
      t.index [:bottle_type_id, :establishment_id], name: :bottle_type_establishment_index
      # t.index [:establishment_id, :draft_type_id]
    end

     create_join_table :sport_types, :establishments do |t|
      t.index [:sport_type_id, :establishment_id], name: :sport_type_establishment_index
      # t.index [:establishment_id, :draft_type_id]
    end
  end
end
