class RemoveIndexFromEstablishments < ActiveRecord::Migration[5.1]
  def change
    remove_index :establishments, :name => "index_establishments_on_name"
  end
end
