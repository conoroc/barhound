class AddHoursToEstablishments < ActiveRecord::Migration[5.1]
	def change
		add_column :establishments, :hours, :jsonb, default: {}
		add_index  :establishments, :hours, using: :gin
	end
end
