class CreateNoiseLevelTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :noise_level_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end
  end
end
