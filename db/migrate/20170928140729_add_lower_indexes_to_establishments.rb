class AddLowerIndexesToEstablishments < ActiveRecord::Migration[5.1]
  def change
    add_index :establishments, "lower(name) varchar_pattern_ops"
    add_index :establishments, "lower(description) varchar_pattern_ops"
  end
end
