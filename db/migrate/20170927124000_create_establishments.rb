class CreateEstablishments < ActiveRecord::Migration[5.1]
  def change
    create_table :establishments do |t|
      t.string :name, null: false
      t.string :phone_number
      t.point :location, :geographic => true
      t.jsonb :address, null: false, default: '{}'
      t.string :username
      t.string :email
      t.string :facebook
      t.string :twitter
      t.string :yelp
      t.string :website
      t.string :tags, array:true, default: []
      t.string :music_types, array:true, default: []
      t.string :best_nights, array:true, default: []
      t.string :description
      t.string :menu_link
      t.integer :suits_dancing
      t.integer :suits_kids
      t.integer :suits_groups
      t.string :drinks_link
      t.boolean :cred_card
      t.boolean :sells_cigs
      t.boolean :free_wifi
      t.boolean :accessible
      t.boolean :high_chair
      t.boolean :coat_check
      t.boolean :dog_friendly
      t.integer :price
      t.string :notes, array: true, default: []
      t.date :founded

      t.timestamps
    end

    # add_reference :establishments, :smoking_area_type, foreign_key: true
    # add_reference :establishments, :beer_garden_type, foreign_key: true
    # add_reference :establishments, :cocktail_type, foreign_key: true
    # add_reference :establishments, :tvs_type, foreign_key: true
    # add_reference :establishments, :wine_type, foreign_key: true
    # add_reference :establishments, :age_type, foreign_key: true
    # add_reference :establishments, :attire_type, foreign_key: true
    # add_reference :establishments, :outdoor_seats_type, foreign_key: true
    # add_reference :establishments, :noise_level, foreign_key: true
    # add_reference :establishments, :parking_type, foreign_key: true
    add_index :establishments, :name, unique: true
    add_index :establishments, :description
    add_index  :establishments, :address, using: :gin
    add_index  :establishments, :notes, using: :gin
    add_index  :establishments, :music_types, using: :gin
    add_index  :establishments, :tags, using: :gin

  end
end
