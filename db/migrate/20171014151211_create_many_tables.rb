class CreateManyTables < ActiveRecord::Migration[5.1]
  def change
     create_table :game_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end

    create_table :draft_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end

    create_table :bottle_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end

    create_table :award_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end

    create_table :sport_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end

    create_table :ambience_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end
  end
end
