class AddReferencesToEstablishments < ActiveRecord::Migration[5.1]
  def change

    add_reference :establishments, :smoking_area_type, foreign_key: true
    add_reference :establishments, :beer_garden_type, foreign_key: true
    add_reference :establishments, :cocktail_type, foreign_key: true
    add_reference :establishments, :tv_type, foreign_key: true
    add_reference :establishments, :wine_type, foreign_key: true
    add_reference :establishments, :age_type, foreign_key: true
    add_reference :establishments, :attire_type, foreign_key: true
    add_reference :establishments, :outdoor_seating_type, foreign_key: true
    add_reference :establishments, :noise_level_type, foreign_key: true
    add_reference :establishments, :parking_type, foreign_key: true

  end
end
