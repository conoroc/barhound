class CreateBeerGardenTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :beer_garden_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end
  end
end
