class CreateParkingTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :parking_types do |t|
      t.string :name
      t.string :description
      t.string :type_id
    end
  end
end
